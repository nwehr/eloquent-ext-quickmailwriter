//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// C++
#include <string>
#include <mutex>

// Boost
#include <boost/property_tree/ptree.hpp>

// Internal
#include "Eloquent/Logging.h"
#include "QuickmailWriter.h"

///////////////////////////////////////////////////////////////////////////////
// RestWriter : IOExtension
///////////////////////////////////////////////////////////////////////////////
Eloquent::RestWriter::QuickmailWriter( const boost::property_tree::ptree::value_type& i_Config
								 , std::mutex& i_LogMutex
								 , streamlog::severity_log& i_Log
								 , std::mutex& i_QueueMutex
								 , std::condition_variable& i_QueueCV
								 , std::queue<QueueItem>& i_Queue
								 , int& i_NumWriters )
: IOExtension( i_Config, i_LogMutex, i_Log, i_QueueMutex, i_QueueCV, i_Queue, i_NumWriters )
, m_To( m_Config.second.get<std::string>( "to" ) )
, m_From( m_Config.second.get<std::string>( "from" ) )
, m_Subject( m_Config.second.get<std::string>( "subject" ) )
, m_SMTPServer( m_Config.second.get<std::string>( "server" ) )
, m_Port( m_Config.second.get<int>( "port" ) )
, m_Username( m_Config.second.get<std::string>( "username" ) )
, m_Password( m_Config.second.get<std::string>( "password" ) )
, m_Quickmail( 0 )
{
	std::unique_lock<std::mutex> LogLock( m_LogMutex );
	m_Log( Eloquent::LogSeverity::SEV_INFO ) << "DatagramWriter::QuickmailWriter() - info - setting up a writer for " << m_To << std::endl;
	
	if( quickmail_initialize() != 0 ) {
		std::unique_lock<std::mutex> LogLock( m_LogMutex );
		m_Log( Eloquent::LogSeverity::SEV_ERROR ) << "DatagramWriter::QuickmailWriter() - error - unable to initialize the quickmail library" << std::endl;
		
		delete this;
		
	}
	
	m_Quickmail = quickmail_create( m_From.data(), m_Subject.data() );
	quickmail_add_to( m_Quickmail, m_To.data() );
	
}

Eloquent::RestWriter::~RestWriter() {
	std::unique_lock<std::mutex> LogLock( m_LogMutex );
	m_Log( Eloquent::LogSeverity::SEV_INFO ) << " - RestWriter::~QuickmailWriter()() - info - shutting down a writer for " << m_To << std::endl;
}

void Eloquent::RestWriter::operator()() {
	boost::optional<int> Batch = m_Config.second.get_optional<int>( "batch" );
	boost::optional<std::string> Filter = m_Config.second.get_optional<std::string>( "filter" );
	
	std::string BatchData	= "";
	int			BatchCount	= 0;
	
	while( true ){
		try {
			QueueItem& Item = NextQueueItem();
			
			if( Batch.is_initialized() ) {
				if( Filter.is_initialized() ) {
					std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
					BatchData.append( m_FilterCoordinator->FilterData( Item.Data() ) );
				} else {
					std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
					BatchData.append( Item.Data() );
				}
				
				if( *Batch == (BatchCount + 1)  ) {
					
					BatchData	= "";
					BatchCount	= 0;
					
				} else ++BatchCount;
				
			} else {
				if( Filter.is_initialized() ) {
					std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
					
					quickmail_set_body( m_Quickmail, Item.Data().data() );
					char Error[1024] = quickmail_send( m_Quickmail, m_Server.data(), m_Port, m_Username.data(), m_Password.data() );
					
					std::cout << "Error: " << Error << std::endl;
					
				} else {
					std::unique_lock<std::mutex> QueueLock( m_QueueMutex );
					
					quickmail_set_body( m_Quickmail, Item.Data().data() );
					char Error[1024] = quickmail_send( m_Quickmail, m_Server.data(), m_Port, m_Username.data(), m_Password.data() );
					
					std::cout << "Error: " << Error << std::endl;
					
				}
				
			}

			PopQueueItem();

		} catch( const std::exception& e ) {
			std::unique_lock<std::mutex> LogLock( m_LogMutex );
			m_Log( Eloquent::LogSeverity::SEV_ERROR ) << " - QuickmailWriter::operator()() - error - " << e.what() << std::endl;
			
		} catch( ... ) {
			std::unique_lock<std::mutex> LogLock( m_LogMutex );
			m_Log( Eloquent::LogSeverity::SEV_ERROR ) << " - QuickmailWriter::operator()() - error - unknown error" << std::endl;
			
		}
		
	}
			
	delete this;
	
}
