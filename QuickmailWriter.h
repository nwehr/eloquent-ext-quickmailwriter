#ifndef _RestWriter_h
#define _RestWriter_h

//
// Copyright 2013-2014 EvriChart, Inc. All Rights Reserved.
// See LICENSE.txt
//

// C++
#include <iostream>

// POCO
#include <Poco/URI.h>

#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>

// External
#include <streamlog/streamlog.h>
#include <quickmail.h>

// Internal
#include "Eloquent/Extensions/IOExtension.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// RestWriter : IOExtension
	///////////////////////////////////////////////////////////////////////////////
	class QuickmailWriter : public IOExtension {
		QuickmailWriter();
		
	public:
		explicit QuickmailWriter( const boost::property_tree::ptree::value_type& i_Config
							, std::mutex& i_LogMutex
							, streamlog::severity_log& i_Log
							, std::mutex& i_QueueMutex
							, std::condition_variable& i_QueueCV
							, std::queue<QueueItem>& i_Queue
							, int& i_NumWriters );

		virtual ~QuickmailWriter();
		virtual void operator()();

	private:
		// Networking
		std::string m_To;
		std::string m_From;
		std::string m_Subject;
		
		std::string m_Server;
		int m_Port; 
		
		std::string m_Username;
		std::string m_Password;
		
		quickmail m_Quickmail; 
		
	};
}

#endif // _RestWriter_h
